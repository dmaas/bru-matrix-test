#!/bin/bash

ATTEN_SCRIPT=/local/repository/bin/atten
RU=$1
ATTEN=$2
BRU1_PATHS=(25 57 89 121 26 58 90 122 27 59 91 123 28 60 92 124)
BRU2_PATHS=(29 30 61 62 93 94 125 126 31 32 63 64 95 96 127 128)
ALL_PATHS=( ${BRU1_PATHS} ${BRU2_PATHS} )

usage() {
    cat <<EOF
usage:
    update-attens -h
        show this help message
    update-attens <ruid> <attenval>
        set all matrix paths between <ruid> and UE to <attenval>
        <ruid> can be bru1, bru2, or both
        <attenval> and be in integer in [0..95]
        actual attenuation is 30+<attenval> dB
EOF
}

set_paths() {
    paths=("$@")
    echo -n "setting paths for $RU to $ATTEN"...
    for path in ${paths[@]}; do
        $ATTEN_SCRIPT $path $ATTEN > /dev/null
    done
    echo "done!"
}

if [ $# -lt 1 ]; then
    usage; exit 0
fi

if [ $# -gt 2 ]; then
    usage; exit 1
fi

if [ $ATTEN -lt 0 ] || [ $ATTEN -gt 95 ]; then
    echo "invalid attenval"; usage; exit 1
fi

case $RU in
    -h)
        usage; exit 0
        ;;
    bru1)
        set_paths ${BRU1_PATHS[@]}
        ;;
    bru2)
        set_paths ${BRU2_PATHS[@]}
        ;;
    both)
        set_paths ${ALL_PATHS[@]}
        ;;
    *)
        echo "invalid arg"; usage; exit 1
        ;;
esac
